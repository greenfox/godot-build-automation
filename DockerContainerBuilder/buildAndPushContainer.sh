#!/bin/sh
set -e

main()
{
    docker build -f Dockerfile --build-arg GODOT_VERSION="${1}" .. -t "$2"
    docker push $2
}


cd $(dirname $0)
main $@